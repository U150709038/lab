package ridvan.shapes;

import java.lang.Math;

public class Circle {
	
	protected int radius;
	
	public Circle() {
		this.radius = 5;
		System.out.println("Circle is being created");
		
	}
	
	public Circle(int radius) {
		this.radius = radius;
		System.out.println("Circle is being created with r="+radius);
	}
	
	public double area() {
		return Math.PI * radius * radius;
	}

}
