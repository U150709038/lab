package ridvan.main;

import ridvan.shapes3d.Cylinder;
import ridvan.shapes3d.Cube;

public class Test3D {

	public static void main(String[] args) {
		Cylinder cylinder = new Cylinder(6, 10);
		System.out.println(cylinder);
		
		Cube cube = new Cube(3, 4, 5);
		System.out.println(cube);
		

	}

}
